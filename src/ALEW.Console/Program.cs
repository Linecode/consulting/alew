﻿// See https://aka.ms/new-console-template for more information

using ALEW.Persistence;
using Microsoft.EntityFrameworkCore;

var context = new VetClinicContext(new DbContextOptions<VetClinicContext>());

context.Database.EnsureDeleted();
context.Database.EnsureCreated();

// HasMany
context.Owners.AsQueryable().Include(x => x.Dogs).ToList();

// ownsMany
context.Owners.AsQueryable().ToList();

Console.WriteLine("Hello, World!");