using ALEW.Persistence.Model;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.Extensions.Logging;

namespace ALEW.Persistence;

public class VetClinicContext : DbContext
{
    public DbSet<Owner> Owners { get; set; }
    public DbSet<Dog> Dogs { get; set; }

    public VetClinicContext(DbContextOptions options) : base(options)
    {
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        base.OnConfiguring(optionsBuilder);

        optionsBuilder.UseSqlServer("Server=localhost,1433;Initial Catalog=vet_clinic;MultipleActiveResultSets=true;User Id=SA;Password=bigStrongPwd(!);Encrypt=False");
        optionsBuilder.EnableSensitiveDataLogging();
        optionsBuilder.UseLoggerFactory(Logger);
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        // modelBuilder.ApplyConfigurationsFromAssembly(typeof(VetClinicContext).Assembly);

        modelBuilder.ApplyConfiguration(new OwnerEntityTypeConfiguration());
    }

    static readonly ILoggerFactory Logger = LoggerFactory.Create(builder => builder.AddConsole());
}

public class OwnerEntityTypeConfiguration : IEntityTypeConfiguration<Owner>
{
    public void Configure(EntityTypeBuilder<Owner> builder)
    {
        builder.ToTable("owner");
        
        builder.HasKey(x => x.Id);
        
        builder.Property(x => x.Notes);
        builder.Property(x => x.FirstName).IsRequired();
        builder.Property(x => x.LastName).IsRequired();
        
        builder.OwnsMany<Dog>(x => x.Dogs, db =>
        {
            db.ToTable("dogs");
            db.Property(d => d.Name);
            db.Property(d => d.Name);
        });
    }
}