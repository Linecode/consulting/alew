namespace ALEW.Persistence.Model;

public class Dog
{
    public int Id { get; set; }
    public string Name { get; set; } = null!;
    public int OwnerId { get; set; }
}