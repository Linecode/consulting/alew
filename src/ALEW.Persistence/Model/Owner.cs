namespace ALEW.Persistence.Model;

public class Owner
{
    public int Id { get; set; }
    public string FirstName { get; set; } = null!;
    public string LastName { get; set; } = null!;
    public string Notes { get; set; } = null!;
    public ICollection<Dog> Dogs { get; set; } = new List<Dog>();
}